import React from "react";
import { Container, Table, Badge, Button } from "react-bootstrap";
import { Component } from "react";

export default class TableData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.items,
    };
  }

  render() {
    let dataWithAmount = this.state.data.filter((item) => item.amount > 0);
    return (
      <Container
        style={{
          marginTop: "20px",
        }}
      >
        <div style={{ marginBottom: "20px" }}>
          <Button
            variant="info"
            className="mx-2"
            onClick={() => this.props.onReset()}
            style={{}}
          >
            Reset
          </Button>
          <Badge
            variant="warning"
            style={{
              height: "24px",
              paddingTop: "6px",
              paddingLeft: "5px",
              paddingRight: "5px",
              textAlign: "center",
              marginLeft: "10px",
            }}
          >
            {"Count : " + dataWithAmount.length}
          </Badge>
        </div>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>#</th>
              <th>Food</th>
              <th>Amount</th>
              <th>Price</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            {dataWithAmount.length == 0 ? (
              <td colSpan="5" style={{ textAlign: "center" }}>
                NO DATA
              </td>
            ) : (
              dataWithAmount.map((item) => (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.title}</td>
                  <td>{item.amount}</td>
                  <td>{item.price + "$"}</td>
                  <td>{item.total + "$"}</td>
                </tr>
              ))
            )}
          </tbody>
        </Table>
      </Container>
    );
  }
}
